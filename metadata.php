<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';
/**
 * Module information
 */
$aModule = [
    'id'           => 'dre_salesconnect',
    'title'        => '<img src="../modules/bender/dre_salesconnect/out/img/favicon.ico" title="Bodynova Admin Sales Connect">odynova Sales Connect',
    'description'  => [
        'de' => 'Modul zur datenübertragung in den Händlershop',
        'en' => 'Modul for data transfer'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'controllers'  => [
        'dre_user_rights'  =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_rights::class
    ],
    'extend'       => [

    ],
    'templates' => [
        'dre_sales_article_list.tpl'  =>
            'bender/dre_salesconnect/Application/views/admin/tpl/dre_sales_article_list.tpl'
    ],
    'settings' => [
    ],
    'events' => [
    ],
    'blocks'      => [
    ]
];

?>
