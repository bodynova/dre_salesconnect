<?php

namespace Bender\dre_SalesConnect\Application\Controller\Admin;

class dre_sales_article_list extends \OxidEsales\Eshop\Application\Controller\Admin\ArticleList
{
    public function render()
    {
        return 'dre_sales_article_list.tpl';
    }
}